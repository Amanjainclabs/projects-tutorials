import UIKit


class ViewController: UIViewController, UITextFieldDelegate {
    
   // Initialised an array to store marks in 5 subjects
    var marks =  [0.0,0.0,0.0,0.0,0.0]
    
    
    // linked various buttons, labels and text fields
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var ageField: UITextField!
    @IBOutlet weak var classField: UITextField!
    @IBOutlet weak var sub1Marks: UITextField!
    @IBOutlet weak var sub2Marks: UITextField!
    @IBOutlet weak var sub3Marks: UITextField!
    @IBOutlet weak var sub4Marks: UITextField!
    @IBOutlet weak var sub5Marks: UITextField!
    
    @IBOutlet weak var errorMessage: UILabel!
    @IBOutlet weak var finalPercentage: UILabel!
    @IBOutlet weak var finalGrade: UILabel!
    
    @IBOutlet weak var imageOne: UIImageView!
    @IBOutlet weak var imageTwo: UIImageView!
    @IBOutlet weak var imageThree: UIImageView!
    
    @IBAction func submitButton(sender: AnyObject) {
        
        
        // checking if any text fiel is empty or not
        if nameField.text != "" && ageField.text != "" && classField.text != "" && sub1Marks.text != "" && sub2Marks.text != "" && sub3Marks.text != "" && sub4Marks.text != "" && sub5Marks.text != "" {
            
            
            // if all fields are full then erase error message
            errorMessage.text = ""
            
            // local var to store sum percentage and to access array
            var sum = 0.0
            var percentage = 0.0
            var index = 0
            
            // storing marks in array
            marks[0] = sub1Marks.text.toDouble()!
            marks[1] = sub2Marks.text.toDouble()!
            marks[2] = sub3Marks.text.toDouble()!
            marks[3] = sub4Marks.text.toDouble()!
            marks[4] = sub5Marks.text.toDouble()!
            
            // calculating sum of marks in all subjects
            for index in 0...4 {
                
                sum += marks[index]
                
            }
            
            percentage = sum / 5.0
            
            // loading the image of star
            var image = UIImage(named: "1216181106356570529jean_victor_balin_icon_star.svg.med.png")
            var blank = UIImage(named: "")
            // grades and stars are given according to the following conditions
            if percentage < 40.0 {
                
                finalGrade.text = "F"
                
            }else if percentage < 60.0 {
                
                finalGrade.text = "D"
            }else if percentage < 75.0 {
                
                finalGrade.text = "C"
                imageOne.image = image
                
            }else if percentage < 90.0 {
                
                finalGrade.text = "B"
                imageOne.image = image
                imageTwo.image = image
                
            }else {
                
                finalGrade.text = "A"
                imageOne.image = image
                imageTwo.image = image
                imageThree.image = image
                
            }
            
            // displaying percentage
            finalPercentage.text = percentage.description
            
        }else {
            var blank = UIImage(named: "")
            // if any textfield is empty then display error message
            errorMessage.text = "Please fill all the details!!!"
            finalPercentage.text = ""
            finalGrade.text = ""
            imageOne.image = blank
            imageTwo.image = blank
            imageThree.image = blank
            
        }
      
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        //  added delegates for the text fields of subjects
        sub1Marks.delegate = self
        sub2Marks.delegate = self
        sub3Marks.delegate = self
        sub4Marks.delegate = self
        sub5Marks.delegate = self
        
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true;
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        
        self.view.endEditing(true)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

// created an extension to convert string to double by name doubleValue
extension String {
    func toDouble() -> Double? {
        var formatter = NSNumberFormatter()
        if let number = formatter.numberFromString(self) {
            return number.doubleValue
        }
        return nil
    }
}